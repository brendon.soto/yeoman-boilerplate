var fs = require('fs'),
    generators = require('yeoman-generator');

module.exports = generators.Base.extend({
  constructor: function() {
    generators.Base.apply(this, arguments);
  },

  writing: function() {
    this.fs.copy(
      this.sourceRoot(),
      this.destinationPath(),
      {
        globOptions: {
          dot: true,
          ignore: [
            '.git',
            'LICENSE',
            'README.md'
          ]
        }
      }
    );
  },

  install: function() {
    this.log('Installing dependencies');
    this.installDependencies({
      bower: false,
      npm: true,
      callback: function() {
        console.log('Dependencies have been installed!');
      }
    });
  }

});
