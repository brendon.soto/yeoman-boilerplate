# Yeoman Generator Shell

This generator was inspired by Addy Osmani's [generator-boilerplate](https://github.com/addyosmani/generator-boilerplate).  
The concept is you use Git submodules as the template for the generator.

## How to use

1. ```git submodule add _your-repo_ generators/app/templates```
2. Install Yeoman ```npm install -g yo```
3. Install the generator ```npm link```
4. Navigate to a directory to create a project
5. Run: ```yo boilerplate```
